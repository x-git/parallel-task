# parallel-task

#### 项目介绍
构建任务依赖图，可多作业并行工作，构建一组任务执行单元，跟踪任务执行时间和状态，可增加页面控制任务是否执行。

#### 关键设计
![任务执行状态控制图](https://gitee.com/uploads/images/2018/0605/234253_7ab798f6_470276.jpeg "任务执行状态控制图.jpg")
<center>图1 任务执行状态控制图</center>


![任务依赖关系概览图](https://gitee.com/uploads/images/2018/0605/234330_01501975_470276.jpeg "任务依赖关系概览图.jpg")
<center>图2 任务依赖关系概览图</center>

#### 关键类图
![任务处理器类图](https://gitee.com/uploads/images/2018/0607/213239_5b46358c_470276.png "任务处理器类图.png")
<center>图3 任务处理器类图</center>


![任务执行单元类图](https://gitee.com/uploads/images/2018/0607/213322_b19281a3_470276.png "任务执行单元类图.png")
<center>图4 任务执行单元类图</center>


#### 特性说明
1. 支持多Job并行执行，各个Job之间是相互独立的，每个Job可自定义任务依赖关系；
2. 支持线程池定制，提供了刷新功能，可根据自己的测试结果，定制线程池的超时策略以及线程池大小、清理策略等；
3. 支持零散任务执行：定制一个线程池，可以执行非Job中的“野”任务；
4. 支持“ForkJoin”特性，根据自定义的切分最小单位，Fork出多个子任务，可提交并行执行；
5. 提供ForkJoin可扩展，重写runWorker方法，可自定义Fork策略以及Join策略（afterExecute）；
6. 可以自定义可视化界面，监控显示所有任务执行状态；
7. 对于已废弃和已执行完成的任务，不再重新执行，执行状态由调用方提供初始化或动态修改；
8. 提供了三种任务关系初始化方式：使用自定义注解、XML配置、数据库存储读取方式

#### 使用说明
请查看src/test/java下com.ngplat.paralleltask.test.spring中的使用示例。

简单说明：
1. 对于普通任务仅需要继承TaskProcessor, 实现runWorker接口即可; 
2. 对于需要拆解子任务的Task，需要继承ForkJoinTask:  
   2.1) 在构造函数中指定业务处理类: super(worker.class);  
   2.2) 重写beforeExecute, 构造数据源;  
   2.3) 重写afterExecute实现Join结果合并;  