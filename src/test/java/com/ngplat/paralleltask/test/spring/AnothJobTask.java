/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: AnothJobTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: AnothJobTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月26日 下午5:09:49
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "1", name = "AnothJobTask", jobId = "SECOND_JOB")
public class AnothJobTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = 4757839304280425681L;

	/**
	 * 创建一个新的实例 AnothJobTask.
	 */
	public AnothJobTask() {
		// TODO Auto-generated constructor stub
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#beforeExecute(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void beforeExecute(TaskContext context) {
		super.beforeExecute(context);
		context.putAttribute("ZCQ", "handsome");
	}
	
	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		System.out.println("[AnothJobTask] Run Anoth Job's Task, JobId: SECOND_JOB, Attribute: " + context.getAttribute("ZCQ"));
	}

}
